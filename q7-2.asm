assume cs:codesg, ds:datasg

datasg segment
    db 'welcome to masm!'
    db '................'
datasg ends

codesg segment
start:mov ax, datasg
      mov ds, ax
      mov bx, 0
      mov cx, 10h
    s:mov al, ds:[bx]
      mov ds:[10h + bx], al

      inc bx
      loop s 

      mov ax, 4c00h
      int 21h
codesg ends
end start
